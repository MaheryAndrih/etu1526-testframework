/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Annotation.MethodAnnotation;
import Utilitaire.ModelView;
import java.util.HashMap;

/**
 *
 * @author mahery
 */
public class Class1 {
    
    @MethodAnnotation(url="helloworld")
    public ModelView helloWorld(){
        ModelView mv = new ModelView();
        mv.setUrl("hello.jsp");
        HashMap<String,Object> data = new HashMap<>();
        data.put("hello", "Hello World EveryBody here ehhhhhhhh!!!!!!!!");
        mv.setData(data);
        return mv;
    }
    
    @MethodAnnotation(url="bye")
    public ModelView veloma(){
        ModelView mv = new ModelView();
        mv.setUrl("bye.jsp");
        HashMap<String,Object> data = new HashMap<>();
        data.put("bye", "Veloma daoly EveryBody here ehhhhhhhh!!!!!!!!");
        mv.setData(data);
        return mv;
    }
    
}
