/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Annotation.MethodAnnotation;
import Utilitaire.ModelView;
import java.util.HashMap;

/**
 *
 * @author mahery
 */
public class Personne {
    
    public String nom;
    public int age;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    @MethodAnnotation(url="formpers")
    public ModelView toFrom(){
        ModelView mv = new ModelView();
        mv.setUrl("formpers.jsp");
        HashMap<String,Object> data = new HashMap<>();
        data.put("data", null);
        mv.setData(data);
        return mv;
    }
    
    @MethodAnnotation(url="detail")
    public ModelView detail(){
        ModelView mv = new ModelView();
        mv.setUrl("detail.jsp");
        HashMap<String,Object> data = new HashMap<>();
        data.put("data", "Nom: "+this.getNom()+" - age: "+this.getAge());
        mv.setData(data);
        return mv;
    }
    
}
