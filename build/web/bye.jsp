<%
    String data = (String) request.getAttribute("bye");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Veloma World!</h1>
        <p><%= data %></p>
    </body>
</html>
